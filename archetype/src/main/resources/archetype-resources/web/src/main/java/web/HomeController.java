#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.${artifactId};

import ${package}.domain.sample.SampleService;
import ${package}.domain.sample.models.Sample;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.${artifactId}.bind.annotation.RequestMapping;
import org.springframework.${artifactId}.bind.annotation.RequestMethod;
import org.springframework.${artifactId}.servlet.ModelAndView;

import java.util.List;

@Controller
public class HomeController {

    private Logger logger = LoggerFactory.getLogger(HomeController.class);

    private SampleService service;

    @Autowired
    public HomeController(SampleService service) {
        this.service = service;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView index() {
        logger.info("Home/Index");
        List<Sample> samples = service.getAll();
        ModelAndView mav = new ModelAndView("index");
        mav.addObject("message", "World");
        mav.addObject("samples", samples);
        return mav;
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public String create() {
        service.create();

        return "redirect:/";
    }
}