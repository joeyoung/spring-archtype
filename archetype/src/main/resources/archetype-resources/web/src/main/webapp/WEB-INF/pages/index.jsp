#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<head>
    <title></title>
</head>
<body>
    <h1>Hello ${symbol_dollar}{message}!</h1>
    <form:form method="post" action="create">
        <input type="submit" value="Create">
    </form:form>
    <table>
        <thead>
            <tr>
                <th>Id</th>
                <th>Created On</th>
                <th>Created By</th>
                <th>Updated On</th>
                <th>Updated By</th>
            </tr>
            <c:forEach var="sample" items="${symbol_dollar}{samples}">
                <tr>
                    <td>${symbol_dollar}{sample.id}</td>
                    <td>${symbol_dollar}{sample.createdOn}</td>
                    <td>${symbol_dollar}{sample.createdBy}</td>
                    <td>${symbol_dollar}{sample.updatedOn}</td>
                    <td>${symbol_dollar}{sample.updatedBy}</td>
                </tr>
            </c:forEach>
        </thead>
    </table>
</body>
</html>
