#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package};

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.${artifactId}.WebAppConfiguration;
import org.springframework.test.${artifactId}.servlet.MockMvc;
import org.springframework.${artifactId}.context.WebApplicationContext;

import static org.springframework.test.${artifactId}.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.${artifactId}.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.${artifactId}.servlet.result.MockMvcResultMatchers.view;
import static org.springframework.test.${artifactId}.servlet.setup.MockMvcBuilders.${artifactId}AppContextSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration("file:/src/main/${artifactId}app/WEB-INF/mvc-dispatcher-servlet.xml")
public class AppTests {
    private MockMvc mockMvc;

    @SuppressWarnings("SpringJavaAutowiringInspection")
    @Autowired
    protected WebApplicationContext wac;

    @Before
    public void setup() {
        this.mockMvc = ${artifactId}AppContextSetup(this.wac).build();
    }

    @Test
    public void simple() throws Exception {
        mockMvc.perform(get("/"))
                .andExpect(status().isOk())
                .andExpect(view().name("hello"));
    }
}
