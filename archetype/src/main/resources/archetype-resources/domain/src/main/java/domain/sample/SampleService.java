#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.${artifactId}.sample;

import ${package}.${artifactId}.sample.models.Sample;

import java.util.List;

/**
 */
public interface SampleService {
    void create();
    List<Sample> getAll();
}
