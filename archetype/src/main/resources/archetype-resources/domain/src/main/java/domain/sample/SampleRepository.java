#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.${artifactId}.sample;

import ${package}.${artifactId}.Repository;
import ${package}.${artifactId}.sample.entities.SampleEntity;
import ${package}.${artifactId}.sample.models.Sample;

import java.util.List;

/**
 */
public interface SampleRepository extends Repository<SampleEntity> {

}
