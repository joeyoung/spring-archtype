#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.${artifactId}.sample.entities;

import ${package}.${artifactId}.AggregateRoot;
import ${package}.${artifactId}.BaseEntity;

import javax.persistence.*;

/**
 */
@Entity
@Table(name = "sample")
@SequenceGenerator(allocationSize = 1, name = "idSequence", sequenceName = "sample_seq")
public class SampleEntity extends BaseEntity implements AggregateRoot {

}
