#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.${artifactId}.sample.impl;

import ${package}.${artifactId}.data.AbstractRepository;
import ${package}.${artifactId}.sample.entities.SampleEntity;
import ${package}.${artifactId}.sample.SampleRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository to handling Sample Aggregate ${parentArtifactId}s
 */
@Repository(value = "sampleRepository")
public class SampleRepositoryImpl extends AbstractRepository<SampleEntity> implements SampleRepository {

    public SampleRepositoryImpl() {
        super(SampleEntity.class);
    }
}
