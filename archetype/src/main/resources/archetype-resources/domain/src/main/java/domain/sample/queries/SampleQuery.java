#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.${artifactId}.sample.queries;

import ${package}.${artifactId}.Query;
import ${package}.${artifactId}.sample.entities.SampleEntity;
import ${package}.${artifactId}.sample.models.Sample;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Root;
import java.util.List;

/**
 * Finds all Samples from the database
 */
public class SampleQuery implements Query<List<Sample>> {
    @Override
    public List<Sample> execute(EntityManager entityManager) {
        // Query via JPA-QL
        return entityManager.createQuery("select new ${package}.${artifactId}.sample.models.Sample(s.id, s.createdOn, s.createdBy, s.updatedOn, s.updatedBy) from SampleEntity s order by createdOn", Sample.class)
                .getResultList();

//        // Query via JPA Criteria
//        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
//
//        // the result is the Sample.class
//        CriteriaQuery<Sample> query = builder.createQuery(Sample.class);
//
//        // start the query at the ${parentArtifactId} (SampleEntity)
//        Root<SampleEntity> ${parentArtifactId} = query.from(SampleEntity.class);
//
//        // define the projection
//        query.multiselect(
//                ${parentArtifactId}.get("id"),
//                ${parentArtifactId}.get("createdOn"),
//                ${parentArtifactId}.get("createdBy"),
//                ${parentArtifactId}.get("updatedOn"),
//                ${parentArtifactId}.get("updatedBy")
//        );
//
//        // order by createdOn ascending
//        query.orderBy(builder.asc(${parentArtifactId}.get("createdOn")));
//
//        // get an "executable" version of the query
//        TypedQuery<Sample> typedQuery = entityManager.createQuery(query);
//
//        // finally get the results
//        List<Sample> results = typedQuery.getResultList();
//
//        return results;
    }
}
