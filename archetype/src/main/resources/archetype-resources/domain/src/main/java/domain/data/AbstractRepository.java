#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.${artifactId}.data;

import ${package}.${artifactId}.AggregateRoot;
import ${package}.${artifactId}.BaseEntity;
import ${package}.${artifactId}.Query;
import ${package}.${artifactId}.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Repository for AggregateRoot's
 *
 * @see ${package}.${artifactId}.AggregateRoot
 * @see ${package}.${artifactId}.BaseEntity
 *
 * @param <T> the BaseEntity and AggregateRoot on which this Repository acts
 */
public abstract class AbstractRepository<T extends BaseEntity & AggregateRoot> implements Repository<T>{

    @PersistenceContext
    private EntityManager entityManager;

    private Class<T> entityClass;

    public AbstractRepository() {}


    protected AbstractRepository(Class<T> entityClass) {
        this.entityClass = entityClass;

    }

    /**
     * Gets the EntityManager associated with this repository
     * @return entityManager
     */
    protected EntityManager getEntityManager() {
        return entityManager;
    }

    /**
     * Adds an AggregateRoot to the Repository
     * @param entity the entity to add
     */
    @Override
    public void add(T entity) {
        entityManager.persist(entity);
    }

    /**
     * Removes an AggregateRoot from the Repository
     * @param entity the entity to remove
     */
    @Override
    public void remove(T entity) {
        entityManager.remove(entityManager.merge(entity));
    }

    /**
     * Gets an AggregateRoot from the Repository by it's identity
     * @param id the identity of the entity / aggregate ${parentArtifactId}
     * @return the entity represented by the id
     */
    @Override
    public T getById(Long id) {
        return entityManager.find(entityClass, id);
    }

    /**
     * Executes a query
     * @param query the query to execute
     * @param <TResponse> the response of the query
     * @return the response of the query
     */
    @Override
    public <TResponse> TResponse execute(Query<TResponse> query) {
        return query.execute(entityManager);
    }
}
