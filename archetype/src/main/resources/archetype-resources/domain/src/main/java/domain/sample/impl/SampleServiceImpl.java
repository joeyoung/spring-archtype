#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.${artifactId}.sample.impl;

import ${package}.${artifactId}.sample.entities.SampleEntity;
import ${package}.${artifactId}.sample.SampleRepository;
import ${package}.${artifactId}.sample.SampleService;
import ${package}.${artifactId}.sample.models.Sample;
import ${package}.${artifactId}.sample.queries.SampleQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 */
@Service
public class SampleServiceImpl implements SampleService {

    private SampleRepository repository;

    @Autowired
    public SampleServiceImpl(SampleRepository repository) {
        this.repository = repository;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void create() {
        SampleEntity sample = new SampleEntity();
        repository.add(sample);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public List<Sample> getAll() {
        return repository.execute(new SampleQuery());
    }
}
