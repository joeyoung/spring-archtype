#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.${artifactId}.data;

import ${package}.${artifactId}.BaseEntity;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

/**
 */
public class AuditEntityListener {

    @PrePersist
    public void onCreate(Object entity) {
        if (entity instanceof BaseEntity) {
            BaseEntity base = (BaseEntity)entity;

            String name = "test";
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

            if (authentication != null) {
                name = authentication.getName();
            }

            DateTime insertDate = new DateTime(DateTimeZone.UTC);

            base.setCreatedBy(name);
            base.setCreatedOn(insertDate);
            base.setUpdatedBy(name);
            base.setUpdatedOn(insertDate);
        }
    }

    @PreUpdate
    public void onUpdate(Object entity) {
        if (entity instanceof BaseEntity) {
            BaseEntity base = (BaseEntity)entity;

            String name = "test";
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

            if (authentication != null) {
                name = authentication.getName();
            }
            DateTime updateTime = new DateTime(DateTimeZone.UTC);

            base.setUpdatedBy(name);
            base.setUpdatedOn(updateTime);
        }
    }
}
