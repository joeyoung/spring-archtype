CURRENT_DIRECTORY = File.dirname(__FILE__)

namespace :db do
  namespace :dev do
    desc 'Migrates the database in the Development profile'
    task :migrate do
      db_migrate 'development'
    end
    desc 'Rolls back the database in the Development profile by 1'
    task :rollback do
      db_rollback 'development'
    end

    desc 'Gets the migration status of the database in the Development profile'
    task :status do
    db_status 'development'
    end
  end

  #-----------------------------------------------------
  # db functions
  #-----------------------------------------------------
  def db_migrate(profile)
    Dir.chdir('domain') do
      sh "mvn liquibase:update -P #{profile}"
    end
  end

  def db_rollback(profile)
    Dir.chdir('domain') do
      sh "mvn liquibase:rollback -Dliquibase.rollbackCount=1 -P #{profile}"
    end
  end

  def db_status(profile)
    Dir.chdir('domain') do
      sh "mvn liquibase:status -P #{profile}"
    end
  end
end
