package com.example.web.tests.unit.controllers;

import com.example.web.controllers.HomeController;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.assertEquals;

/**
 */
@RunWith(JUnit4.class)
public class HomeControllerTests {

    private HomeController controller;

    @Before
    public void setup() {
        controller = new HomeController();
    }

    @Test
    public void get_index_should_return_the_correct_view() {
        String response = controller.index();

        assertEquals("home/index", response);
    }
}
