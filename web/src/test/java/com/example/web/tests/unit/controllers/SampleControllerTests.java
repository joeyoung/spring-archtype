package com.example.web.tests.unit.controllers;

import com.example.domain.sample.SampleService;
import com.example.domain.sample.models.Sample;
import com.example.web.controllers.SamplesController;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 */
@RunWith(JUnit4.class)
public class SampleControllerTests {

    private SampleService sampleServiceMock;
    private SamplesController controller;

    @Before
    public void setup() {
        sampleServiceMock = mock(SampleService.class);
        controller = new SamplesController(sampleServiceMock);
    }

    @Test
    public void get_index_should_set_model_data_and_return_view() {
        Model modelMock = mock(Model.class);
        List<Sample> samples = new ArrayList<>();

        when(sampleServiceMock.getAll()).thenReturn(samples);

        String view = controller.index(modelMock);

        verify(modelMock).addAttribute("message", "World");
        verify(modelMock).addAttribute("samples", samples);

        assertEquals("samples/index", view);
    }

    @Test
    public void get_create_should_add_the_model_and_return_the_view() {
        Model modelMock = mock(Model.class);

        String view = controller.create(modelMock);

        verify(modelMock).addAttribute(eq("sample"), any(Sample.class));

        assertEquals("samples/create", view);
    }

    @Test
    public void get_create_should_not_add_the_model_if_it_already_exists() {
        Model model = new ExtendedModelMap();
        Sample sample = new Sample();

        model.addAttribute("sample", sample);

        String view = controller.create(model);

        assertEquals(sample, model.asMap().get("sample"));
        assertEquals("samples/create", view);
    }

    @Test
    public void post_create_should_create_the_sample_and_redirect_to_the_samples_view() {
        BindingResult bindingResultMock = mock(BindingResult.class);
        RedirectAttributes attributesMock = mock(RedirectAttributes.class);
        Sample sample = new Sample();

        when(bindingResultMock.hasErrors()).thenReturn(false);

        String response = controller.create(sample, bindingResultMock, attributesMock);

        verify(sampleServiceMock).create(sample);
        assertEquals("redirect:/samples", response);
    }

    @Test
    public void post_create_should_redirect_to_create_and_set_redirect_attributes_if_there_are_binding_errors() {
        BindingResult bindingResultMock = mock(BindingResult.class);
        RedirectAttributes attributesMock = mock(RedirectAttributes.class);
        Sample sample = new Sample();

        when(bindingResultMock.hasErrors()).thenReturn(true);

        String response = controller.create(sample, bindingResultMock, attributesMock);

        verify(attributesMock).addFlashAttribute(eq(BindingResult.MODEL_KEY_PREFIX + "sample"), eq(bindingResultMock));
        verify(attributesMock).addFlashAttribute(eq("sample"), eq(sample));

        assertEquals("redirect:/samples/create", response);
    }
}
