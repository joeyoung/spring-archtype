package com.example.web.controllers;

import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

public abstract class BaseController {
    public void setRedirectionAttributes(RedirectAttributes attributes, String name, Object model, BindingResult bindingResult) {
        attributes.addFlashAttribute(BindingResult.MODEL_KEY_PREFIX + name, bindingResult);
        attributes.addFlashAttribute(name, model);
    }
}
