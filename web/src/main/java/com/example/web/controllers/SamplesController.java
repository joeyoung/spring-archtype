package com.example.web.controllers;

import com.example.domain.sample.SampleService;
import com.example.domain.sample.models.Sample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
public class SamplesController extends BaseController {

    private SampleService sampleService;

    @Autowired
    public SamplesController(SampleService sampleService) {
        this.sampleService = sampleService;
    }

    @RequestMapping(value = "/samples", method = RequestMethod.GET)
    public String index(Model model) {

        // add attributes to the model, these are accessible from the view
        model.addAttribute("message", "World");
        model.addAttribute("samples", sampleService.getAll());

        return "samples/index";
    }

    @RequestMapping(value = "/samples/create", method = RequestMethod.GET)
    public String create(Model model) {

        // bind any objects needed for the view such as drop down lists, tables etc.

        // the post action may have already given us the model, and if so, we don't want to override it
        if (!model.containsAttribute("sample")) {
            // add a default model, could set default values as well
            model.addAttribute("sample", new Sample());
        }

        return "samples/create";
    }


    @RequestMapping(value = "/samples/create", method = RequestMethod.POST)
    public String create(@Valid @ModelAttribute("sample") Sample sample, BindingResult bindingResult, RedirectAttributes attributes) {
        if (bindingResult.hasErrors()) {
            // the binding result has errors so we need to return to the create view
            // the redirect attributes will keep the binding result and model state so that they can be presented back on the view
            setRedirectionAttributes(attributes, "sample", sample, bindingResult);
            return "redirect:/samples/create";
        }

        sampleService.create(sample);

        return "redirect:/samples";
    }
}
