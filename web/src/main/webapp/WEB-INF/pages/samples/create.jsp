<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<head>
    <title></title>
</head>
<body>
    <form:form method="post" action="create" modelAttribute="sample">
        <form:input path="name" />
        <form:errors path="name" />
        <input type="submit" value="Create">
    </form:form>
</body>
</html>
