<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<head>
    <title></title>
</head>
<body>
<h1>Hello ${message}!</h1>
    <a href="<c:url value="/samples/create" />">Create</a>
<table>
    <thead>
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Created On</th>
        <th>Created By</th>
        <th>Updated On</th>
        <th>Updated By</th>
    </tr>
    <c:forEach var="sample" items="${samples}">
        <tr>
            <td>${sample.id}</td>
            <td>${sample.name}</td>
            <td>${sample.createdOn}</td>
            <td>${sample.createdBy}</td>
            <td>${sample.updatedOn}</td>
            <td>${sample.updatedBy}</td>
        </tr>
    </c:forEach>
    </thead>
</table>
</body>
</html>