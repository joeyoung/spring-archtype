package com.example.domain.tests.integration.sample;

import com.example.domain.sample.SampleRepository;
import com.example.domain.sample.entities.SampleEntity;
import com.example.domain.tests.integration.TransactionalIntegrationTest;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 */
public class SampleRepositoryTests extends TransactionalIntegrationTest {

    @Autowired
    SampleRepository repository;

    @PersistenceContext
    EntityManager entityManager;

    @Test
    public void add_persists_the_entity() {
        repository.add(new SampleEntity("Test"));

        long count = entityManager.createQuery("select count(*) from SampleEntity", Long.class).getSingleResult();

         assertEquals(1, count);
    }

    @Test
    public void remove_removed_the_entity() {
        SampleEntity sample = new SampleEntity("Test");

        repository.add(sample);
        entityManager.flush(); // force the write

        repository.remove(sample);


        long count = entityManager.createQuery("select count(*) from SampleEntity", Long.class).getSingleResult();

        assertEquals(0, count);
    }

    @Test
    public void getById_gets_the_entity() {
        SampleEntity sample = new SampleEntity("Test");

        repository.add(sample);
        entityManager.flush(); // force the write

        SampleEntity s = repository.getById(sample.getId());
        assertNotNull(s);
    }
}
