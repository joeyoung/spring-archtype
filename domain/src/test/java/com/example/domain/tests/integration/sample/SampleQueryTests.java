package com.example.domain.tests.integration.sample;

import com.example.domain.sample.entities.SampleEntity;
import com.example.domain.sample.models.Sample;
import com.example.domain.sample.queries.SampleQuery;
import com.example.domain.tests.integration.TransactionalIntegrationTest;
import org.junit.Before;
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 */
public class SampleQueryTests extends TransactionalIntegrationTest {
    @PersistenceContext
    private EntityManager entityManager;

    private SampleQuery query;

    @Before
    public void setup() {
        query = new SampleQuery();
    }

    @Test
    public void execute_should_return_samples() {
        entityManager.persist(new SampleEntity("Test"));
        entityManager.persist(new SampleEntity("Test"));

        entityManager.flush();

        List<Sample> samples = query.execute(entityManager);

        assertEquals(2, samples.size());
    }
}
