package com.example.domain.tests.unit.sample;

import com.example.domain.sample.SampleRepository;
import com.example.domain.sample.entities.SampleEntity;
import com.example.domain.sample.impl.SampleServiceImpl;
import com.example.domain.sample.models.Sample;
import com.example.domain.sample.queries.SampleQuery;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

/**
 */
@RunWith(JUnit4.class)
public class SampleServiceTests {
    SampleRepository repositoryMock;
    SampleServiceImpl service;

    @Before
    public void setup() {
        repositoryMock = mock(SampleRepository.class);
        service = new SampleServiceImpl(repositoryMock);
    }

    @Test
    public void create_should_add_the_entity_to_the_repository() {
        service.create(new Sample());
        verify(repositoryMock).add(any(SampleEntity.class));
    }

    @Test
    public void getAll_should_get_all_samples() {
        when(repositoryMock.execute(any(SampleQuery.class))).thenReturn(new ArrayList<Sample>());

        List<Sample> samples = service.getAll();

        assertNotNull(samples);
    }
}
