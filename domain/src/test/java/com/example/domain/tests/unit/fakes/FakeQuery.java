package com.example.domain.tests.unit.fakes;

import com.example.domain.Query;

import javax.persistence.EntityManager;

/**
*/
public class FakeQuery implements Query<FakeEntity> {
    @Override
    public FakeEntity execute(EntityManager entityManager) {
        return null;
    }
}
