package com.example.domain.tests.unit.data;

import com.example.data.AuditEntityListener;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import com.example.domain.tests.unit.fakes.FakeEntity;

import java.util.Collection;

import static org.junit.Assert.*;

/**
 */
@RunWith(JUnit4.class)
public class AuditEntityListenerTests {

    private AuditEntityListener listener;

    @Before
    public void setup() {
        listener = new AuditEntityListener();
        SecurityContextHolder.getContext().setAuthentication(new Authentication() {
            @Override
            public Collection<? extends GrantedAuthority> getAuthorities() {
                return null;
            }

            @Override
            public Object getCredentials() {
                return null;
            }

            @Override
            public Object getDetails() {
                return null;
            }

            @Override
            public Object getPrincipal() {
                return null;
            }

            @Override
            public boolean isAuthenticated() {
                return false;
            }

            @Override
            public void setAuthenticated(boolean b) throws IllegalArgumentException {

            }

            @Override
            public String getName() {
                return "Test";
            }
        });
    }

    @After
    public void tearDown() {
        SecurityContextHolder.clearContext();
    }

    @Test
    public void onCreate_should_set_the_audit_fields() {
        FakeEntity entity = new FakeEntity();

        listener.onCreate(entity);

        assertNotNull(entity.getCreatedOn());
        assertNotNull(entity.getUpdatedOn());
        assertEquals("Test", entity.getCreatedBy());
        assertEquals("Test", entity.getUpdatedBy());
    }

    @Test
    public void onUpdate_should_set_the_audit_fields() {
        FakeEntity entity = new FakeEntity();

        listener.onUpdate(entity);

        assertNotNull(entity.getUpdatedOn());
        assertEquals("Test", entity.getUpdatedBy());

        // it should not set the created fields
        assertNull(entity.getCreatedOn());
        assertNull(entity.getCreatedBy());

    }
}
