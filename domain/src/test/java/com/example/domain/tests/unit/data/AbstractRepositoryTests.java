package com.example.domain.tests.unit.data;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import com.example.domain.tests.unit.fakes.FakeEntity;
import com.example.domain.tests.unit.fakes.FakeQuery;
import com.example.domain.tests.unit.fakes.FakeRepository;

import javax.persistence.EntityManager;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.*;

/**
 */
@RunWith(JUnit4.class)
public class AbstractRepositoryTests {

    private FakeRepository repository;
    private EntityManager entityManagerMock;

    @Before
    public void setup() {
        entityManagerMock = mock(EntityManager.class);
        repository = new FakeRepository();
        repository.setEntityManager(entityManagerMock);
    }


    @Test
    public void get_entityManger_should_return_the_entityManager() {

        assertEquals(entityManagerMock, repository.getEntityManager());
    }

    @Test
    public void add_should_add_the_entity_to_the_entityManager() {
        FakeEntity entity = new FakeEntity();

        repository.add(entity);
        verify(entityManagerMock).persist(entity);
    }

    @Test
    public void remove_should_remove_the_entity_from_the_entityManager() {
        FakeEntity entity = new FakeEntity();
        when(entityManagerMock.merge(entity)).thenReturn(entity);

        repository.remove(entity);

        verify(entityManagerMock).merge(entity);
        verify(entityManagerMock).remove(entity);
    }

    @Test
    public void getById_should_find_the_entity_with_the_entityManager() {
        FakeEntity entity = new FakeEntity();
        when(entityManagerMock.find(FakeEntity.class, 1L)).thenReturn(entity);

        FakeEntity response = repository.getById(1L);

        assertEquals(entity, response);
    }

    @Test
    public void execute_should_execute_the_query_with_the_entityManager() {
        FakeQuery queryMock = mock(FakeQuery.class);

        repository.execute(queryMock);

        verify(queryMock).execute(entityManagerMock);
    }

}
