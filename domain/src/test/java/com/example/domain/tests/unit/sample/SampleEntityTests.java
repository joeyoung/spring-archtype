package com.example.domain.tests.unit.sample;

import com.example.domain.sample.entities.SampleEntity;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class SampleEntityTests {

    @Test
    public void constructor_should_set_args() {
        SampleEntity sample = new SampleEntity("test");
        assertEquals("test", sample.getName());
    }
}
