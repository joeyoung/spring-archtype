package com.example.domain.tests.unit.fakes;

import com.example.data.AbstractRepository;

/**
*/
public class FakeRepository extends AbstractRepository<FakeEntity> {

    public FakeRepository() {
        super(FakeEntity.class);
    }
}
