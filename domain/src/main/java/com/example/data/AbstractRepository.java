package com.example.data;

import com.example.domain.AggregateRoot;
import com.example.domain.BaseEntity;
import com.example.domain.Query;
import com.example.domain.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Repository for AggregateRoot's
 *
 * @see com.example.domain.AggregateRoot
 * @see com.example.domain.BaseEntity
 *
 * @param <T> the BaseEntity and AggregateRoot on which this Repository acts
 */
public abstract class AbstractRepository<T extends BaseEntity & AggregateRoot> implements Repository<T>{

    @PersistenceContext
    private EntityManager entityManager;

    private Class<T> entityClass;

    private AbstractRepository() {}


    protected AbstractRepository(Class<T> entityClass) {
        this.entityClass = entityClass;

    }

    /**
     * Sets the repository's EntityManager
     * @param entityManager the EntityManager for this repository
     */
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    /**
     * Gets the EntityManager associated with this repository
     * @return entityManager
     */
    public EntityManager getEntityManager() {
        return entityManager;
    }

    /**
     * Adds an AggregateRoot to the Repository
     * @param entity the entity to add
     */
    @Override
    public void add(T entity) {
        entityManager.persist(entity);
    }

    /**
     * Removes an AggregateRoot from the Repository
     * @param entity the entity to remove
     */
    @Override
    public void remove(T entity) {
        entityManager.remove(entityManager.merge(entity));
    }

    /**
     * Gets an AggregateRoot from the Repository by it's identity
     * @param id the identity of the entity / aggregate root
     * @return the entity represented by the id
     */
    @Override
    public T getById(Long id) {
        return entityManager.find(entityClass, id);
    }

    /**
     * Executes a query
     * @param query the query to execute
     * @param <TResponse> the response of the query
     * @return the response of the query
     */
    @Override
    public <TResponse> TResponse execute(Query<TResponse> query) {
        return query.execute(entityManager);
    }
}
