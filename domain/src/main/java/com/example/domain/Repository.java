package com.example.domain;

/**
 * Repository for AggregateRoot's
 *
 * @see AggregateRoot
 * @see BaseEntity
 *
 * @param <T> the BaseEntity and AggregateRoot on which this Repository acts
 */
public interface Repository<T extends BaseEntity & AggregateRoot> {
    /**
     * Adds an AggregateRoot to the Repository
     * @param entity the entity to add
     */
    void add(T entity);

    /**
     * Removes an AggregateRoot from the Repository
     * @param entity the entity to remove
     */
    void remove(T entity);

    /**
     * Gets an AggregateRoot from the Repository by it's identity
     * @param id the identity of the entity / aggregate root
     * @return the entity represented by the id
     */
    T getById(Long id);

    /**
     * Executes a query
     * @param query the query to execute
     * @param <TResponse> the response of the query
     * @return the response of the query
     */
    <TResponse> TResponse execute(Query<TResponse> query);
}
