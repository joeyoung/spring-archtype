package com.example.domain;

import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

import javax.persistence.*;

/**
 */
@MappedSuperclass
public abstract class BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSequence") // idSequence is the name of the generate on the entity itself
    private Long id;

    @Column(nullable = false, updatable = false)
    @Type(type="org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    private DateTime createdOn;

    @Column(nullable = false)
    @Type(type="org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    private DateTime updatedOn;

    @Column(nullable = false, updatable = false)
    private String createdBy;

    @Column(nullable = false)
    private String updatedBy;

    /**
     * Gets the identity of the entity. The identity is generated by the db and as such may not be available
     * until the entity is persisted.
     *
     * @return the identity of the entity
     */
    public Long getId() {
        return id;
    }

    /**
     * Gets the date this entity was created on in UTC
     *
     * @return date created
     */
    public DateTime getCreatedOn() {
        return createdOn;
    }

    /**
     * Sets the date this entity was created on. Should be set by the Hibernate Interceptor but can be overridden here.
     *
     * @param createdOn the date to set the value to
     */
    public void setCreatedOn(DateTime createdOn) {
        this.createdOn = createdOn;
    }

    /**
     * Get the date this entity was updated on in UTC.
     *
     * @return the date this entity was updated on
     */
    public DateTime getUpdatedOn() {
        return updatedOn;
    }

    /**
     * Set the date this entity was updated on. Should be set by the Hibernate Interceptor but can be overridden here.
     *
     * @param updatedOn the date to set the value to
     */
    public void setUpdatedOn(DateTime updatedOn) {
        this.updatedOn = updatedOn;
    }

    /**
     * Gets the username of the user that created this entity.
     *
     * @return the username of the user that created this entity
     */
    public String getCreatedBy() {
        return createdBy;
    }


    /**
     * Sets the username of the user that created this entity. Should ALWAYS be set by the Hibernate Interceptor.
     * @param createdBy the value to set who created this entity
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    /**
     * Gets the username of the user that updated this entity.
     * @return the username of the user that updated this entity
     */
    public String getUpdatedBy() {
        return updatedBy;
    }

    /**
     * Sets the username of the user that updated this entity. Should ALWAYS be set by the Hibernate Interceptor.
     * @param updatedBy the value to set who updated this entity
     */
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }
}
