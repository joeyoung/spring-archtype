package com.example.domain;

/**
 * Marker interface for any Domain Entity that is an AggregateRoot.
 *
 * The AggregateRoot is an implementation of DDD and is responsible for controlling the actions of all Entities within the Root.
 */
public interface AggregateRoot {
}
