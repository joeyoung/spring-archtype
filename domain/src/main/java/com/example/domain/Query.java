package com.example.domain;

import javax.persistence.EntityManager;

/**
 */
public interface Query<TResponse> {
    TResponse execute(EntityManager entityManager);
}
