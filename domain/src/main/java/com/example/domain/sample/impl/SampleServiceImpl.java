package com.example.domain.sample.impl;

import com.example.domain.sample.SampleRepository;
import com.example.domain.sample.SampleService;
import com.example.domain.sample.entities.SampleEntity;
import com.example.domain.sample.models.Sample;
import com.example.domain.sample.queries.SampleQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 */
@Service
public class SampleServiceImpl implements SampleService {

    private SampleRepository repository;

    @Autowired
    public SampleServiceImpl(SampleRepository repository) {
        this.repository = repository;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void create(Sample model) {
        SampleEntity sample = new SampleEntity(model.getName());
        repository.add(sample);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public List<Sample> getAll() {
        return repository.execute(new SampleQuery());
    }
}
