package com.example.domain.sample.queries;

import com.example.domain.Query;
import com.example.domain.sample.entities.SampleEntity;
import com.example.domain.sample.models.Sample;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Root;
import java.util.List;

/**
 * Finds all Samples from the database
 */
public class SampleQuery implements Query<List<Sample>> {
    @Override
    public List<Sample> execute(EntityManager entityManager) {
        // Query via JPA-QL
//        return entityManager.createQuery("select new com.example.domain.sample.models.Sample(s.id, s.createdOn, s.createdBy, s.updatedOn, s.updatedBy) from SampleEntity s order by createdOn", Sample.class)
//                .getResultList();

        // Query via JPA Criteria
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();

        // the result is the Sample.class
        CriteriaQuery<Sample> query = builder.createQuery(Sample.class);

        // start the query at the root (SampleEntity)
        Root<SampleEntity> root = query.from(SampleEntity.class);

        // define the projection
        query.multiselect(
                root.get("id"),
                root.get("name"),
                root.get("createdOn"),
                root.get("createdBy"),
                root.get("updatedOn"),
                root.get("updatedBy")
        );

        // order by createdOn ascending
        query.orderBy(builder.asc(root.get("createdOn")));

        // get an "executable" version of the query
        TypedQuery<Sample> typedQuery = entityManager.createQuery(query);

        // finally get the results
        List<Sample> results = typedQuery.getResultList();

        return results;
    }
}
