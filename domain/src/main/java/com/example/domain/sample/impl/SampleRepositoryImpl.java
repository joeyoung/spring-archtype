package com.example.domain.sample.impl;

import com.example.data.AbstractRepository;
import com.example.domain.sample.entities.SampleEntity;
import com.example.domain.sample.SampleRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository to handling Sample Aggregate roots
 */
@Repository(value = "sampleRepository")
public class SampleRepositoryImpl extends AbstractRepository<SampleEntity> implements SampleRepository {

    public SampleRepositoryImpl() {
        super(SampleEntity.class);
    }
}
