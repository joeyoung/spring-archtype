package com.example.domain.sample;

import com.example.domain.Repository;
import com.example.domain.sample.entities.SampleEntity;
import com.example.domain.sample.models.Sample;

import java.util.List;

/**
 */
public interface SampleRepository extends Repository<SampleEntity> {

}
