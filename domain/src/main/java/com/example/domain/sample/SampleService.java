package com.example.domain.sample;

import com.example.domain.sample.models.Sample;

import java.util.List;

/**
 */
public interface SampleService {
    void create(Sample sample);
    List<Sample> getAll();
}
